import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

function getNestedFiles (items) {
  var result = []
  for (var i = 0; i < items.length; i++) {
    if (items[i].type === 'file') {
      result.push(items[i].name)
    }
  }
  return result
}

// из json'а переносим файлы с типом "file" в отдельный узел, оставляем в children только папки для отрисовки в treeview
function getTreeItems (items, state) {
  var result = []
  for (var i = 0; i < items.length; i++) {
    var obj = items[i]
    if (obj.type === 'directory') {
      state.sequence++
      obj.id = state.sequence
      if (obj.children) {
        state.filesInFolder[state.sequence] = getNestedFiles(obj.children)
        var childrens = getTreeItems(obj.children, state)
        if (childrens.length > 0) {
          obj.children = childrens
        } else {
          delete obj.children
        }
      }
      result.push(obj)
    }
  }
  return result
}

export default new Vuex.Store({
  state: {
    fileStructure: [],
    filesInFolder: [],
    remoteServer: 'http://127.0.0.1:8081',
    activeTreeNode: [],
    sequence: 1,
    loading: false,
    overlay: false,
    zIndex: 10
  },
  mutations: {
    SET_FILESTRUCTURE: (state, filestruct) => {
      state.fileStructure = getTreeItems(filestruct, state)
    },
    COMMIT_ACTIVE_TREE_NODE: (state, node) => {
      state.activeTreeNode = node
    },
    COMMIT_DELETE_FILE: (state, index) => {
      state.filesInFolder[state.activeTreeNode].splice(index, 1)
      Vue.set(state.filesInFolder, state.activeTreeNode, state.filesInFolder[state.activeTreeNode])
    },
    COMMIT_SET_OVERLAY: (state) => {
      state.overlay = !state.overlay
    }
  },
  actions: {
    GET_REMOTE_FILESTRUCTURE ({ commit, getters }) {
      return axios(getters.REMOTEURL + '/readdir', {
        method: 'GET'
      })
        .then((filestruct) => {
          commit('SET_FILESTRUCTURE', filestruct.data.children)
        })
    },
    SET_ACTIVE_TREE_NODE ({ commit }, node) {
      commit('COMMIT_ACTIVE_TREE_NODE', node)
    },
    DELETE_FILE ({ commit }, index) {
      // тут еще надо дернуть апишку по удалению
      commit('COMMIT_DELETE_FILE', index)
    },
    SET_OVERLAY ({ commit }) {
      commit('COMMIT_SET_OVERLAY')
    }
  },
  getters: {
    FILESTRUCTURE (state) {
      return state.fileStructure
    },
    REMOTEURL (state) {
      return state.remoteServer
    },
    FILESINFOLDER (state) {
      return state.filesInFolder
    },
    ACTIVEFILES (state) {
      return state.filesInFolder[state.activeTreeNode]
    },
    ISLOADING (state) {
      return state.loading
    },
    ISOVERLAY (state) {
      return state.overlay
    },
    GETZINDEX (state) {
      return state.zIndex
    }
  }
})
